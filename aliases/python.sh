function py() {
    if [ $# -eq 0 ]; then
        docker run -it --rm python:alpine
    elif [ $1 = "-f" ]; then
        if [ $# -eq 1 ]; then
            if test -f "$PWD/requirements.txt"; then
                docker run -it -v $PWD:/src -w="/src" --rm python:slim \
                    /bin/bash -c "pip install --upgrade pip; pip install -r requirements.txt; /bin/bash"
            else
                docker run -it -v $PWD:/src -w="/src" --rm python:slim \
                    /bin/bash -c "pip install --upgrade pip; /bin/bash"
            fi
        else
            if test -f "$PWD/requirements.txt"; then
                docker run -it -v $PWD:/src -w="/src" --rm python:slim \
                    /bin/bash -c "pip install --upgrade pip; pip install -r requirements.txt; ${@:2}"
            else
                docker run -it -v $PWD:/src -w="/src" --rm python:slim \
                    /bin/bash -c "pip install --upgrade pip; ${@:2}"
            fi
        fi
    else
        cat $1 | docker run -i --rm python:alpine
    fi
}

