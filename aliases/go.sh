
function go() {
    if [[ -z $GOPATH ]]; then
        echo
        echo '*************************'
        echo '*     W A R N I N G     *'
        echo '* ----------------------*'
        echo '* \$GOPATH not set      *'
        echo '* defaulting to user/go *'
        echo '*************************'
        echo
        export GOPATH=$PWD/go
    fi
    docker run -i --rm -v $GOPATH:/go golang:alpine /bin/sh -c "go $@"
}

