##############################
# M E T A S P L O I T 
##############################

msfconsole() {
    if [ ! "$(docker ps -q -f name=msfconsole)" ]; then
        if [ "$(docker ps -aq -f status=exited -f name=msfconsole)" ]; then
            docker rm msfconsole
        fi
        docker pull metasploitframework/metasploit-framework:latest
        docker run -it --rm --name msfconsole metasploitframework/metasploit-framework:latest
    fi
}
